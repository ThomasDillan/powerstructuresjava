

/**
 * 
 * Generic Sparse Matrix
 */
public class SparseMatrix<Type>{
	class Cell{
		Cell right, bottom;
		int line, column;
		Type value;
		
		public Cell() {}
		public Cell(int line, int column, Type value){
			this.line = line;
			this.column = column;
			this.value = value;
		}
		public Cell(Cell right, Cell bottom, int line, int column, Type value){
			this.right = right!=null ? right : this;
			this.bottom = bottom!=null ? bottom : this;
			this.line = line;
			this.column = column;
			this.value = value;
		}
	}
	
	private int m=0, n=0, size=0;
	private Cell first, aux;
	
	public SparseMatrix() {
		this.first = new Cell(-1, -1, null);
		this.first.bottom = this.first;
		this.first.right = this.first;
	}
	public SparseMatrix(int m, int n){
		this.first = new Cell(-1, -1, null);
		this.first.bottom = this.first;
		this.first.right = this.first;
		
		initialize(m,n);
	}
	
	public int getM() {
		return m;
	}
	public int getN() {
		return n;
	}
	public Cell getFirst(){
		return this.first;
	}
	public int getSize(){
		return this.size;
	}
	
	private void setM(int m) {
		this.m = m;
	}
	private void setN(int n) {
		this.n = n;
	}
	
	/**
	 * Creates the base to insert elements
	 * 
	 * @param m - number of lines
	 * @param n - number of columns
	 * 
	 * @author Marcos Paulo
	 */
 	public void initialize(int m, int n){
		setM(m);
		setN(n);
		
		getFirst().bottom = null;
		getFirst().right = null;
		
		Cell c;
		int cont;
		
		aux = getFirst();
		for(cont=1; cont<=getM(); cont++){
			c = new Cell(null, getFirst(), cont, -1, null);
			aux = aux.bottom = c;
		}
		
		aux = getFirst();
		for(cont=1; cont<=getN(); cont++){
			c = new Cell(getFirst(), null, -1, cont, null);
			aux = aux.right = c;
		}
	}

 	/**
 	 * Returns the left element of a position(i,j) 
 	 * 
 	 * @param i - line 
 	 * @param j - column
 	 * 
 	 * @return Cell
 	 * 
 	 * @author Marcos Paulo
 	 */
 	private Cell leftElement(int i, int j){
		aux = getFirst();
		while(aux.bottom.line != -1){
			aux = aux.bottom;
			if(aux.line==i){
				while(aux.right.column != -1){
					if(aux.right.column >= j)
						break;
					aux = aux.right;
				}
				break;
			}
		}
		return aux;
 	}
 	/**
 	 * Returns the top element of a position(i,j) 
 	 * 
 	 * @param i - line 
 	 * @param j - column
 	 * 
 	 * @return Cell
 	 * 
 	 * @author Marcos Paulo
 	 */
 	private Cell topElement(int i, int j){
		aux = getFirst();
		while(aux.right.column != -1){
			aux = aux.right;
			if(aux.column==j){
				while(aux.bottom.line != -1){
					if(aux.bottom.line >= i)
						break;
					aux = aux.bottom;
				}
				break;
			}
		}
		return aux;
 	}
 	
 	/**
 	 * Returns the element's value on a position(i,j)
 	 * 
 	 * @param i - line 
 	 * @param j - column
 	 * 
 	 * @return Type generic
 	 * 
 	 * @author Marcos Paulo
 	 */
 	public Type get(int i, int j){
		aux = leftElement(i, j).right;
		if(aux.line == i && aux.column == j)
			return aux.value;
		return null;
 	}

 	/**
 	 * Insert an element 
 	 * 
 	 * @param i - line
 	 * @param j - column
 	 * @param value - element's value
 	 * 
 	 * @throws Exception when the insertion position are invalid
 	 * 
 	 * @author Marcos Paulo
 	 */
	public void insert(int i, int j, Type value) throws Exception{
		if(i > 0 && j > 0 && i<=getM() && j<=getN()){
			if(value != null){
				Cell c = new Cell(i, j, value);
				
				aux = leftElement(i, j);
				c.right = aux.right;
				aux.right = c;
				
				aux = topElement(i, j);
				c.bottom = aux.bottom;
				aux.bottom = c;
				
				this.size++;
			}
		}else
			throw new Exception("Error: invalid position!");
	}
	
	/**
 	 * Remove an element 
 	 * 
 	 * @param i - line
 	 * @param j - column
 	 * 
 	 * @return Type generic
 	 * 
 	 * @author Marcos Paulo
 	 */
	public Type remove(int i, int j){
		aux = leftElement(i, j);
		if(aux.right.column==j){
			Type f = aux.right.value;
			aux.right = aux.right.right;
			
			aux = topElement(i, j);
			aux.bottom = aux.bottom.bottom;
			
			return f;
		}else
			return null;
	}
	
	/**
 	 * Print the matrix cells
 	 * 
 	 * @author Marcos Paulo
 	 */
	public void printMatrix(){
		System.out.println("Matrix:");
		aux = getFirst();
		Cell aux2, none= new Cell();
		int line, column;
		for(line=1; line<=getM(); line++){
			aux2 = aux = aux.bottom;
			for(column=1; column<=getN(); column++){
				if(aux2.right.column == column){
					aux2 = aux2.right;
					printCell(aux2);
				}else{
					printCell(none);
				}
			}
			System.out.println();
		}
	}
	/**
 	 * Print a cell
 	 * 
 	 * @param c - cell
 	 * 
 	 * @author Marcos Paulo
 	 */
	private void printCell(Cell c){
		if(c != null)
			System.out.print(c.line+","+c.column+" -> "+c.value+"\t");
		else
			System.out.println("NULL");
	}
	
	/**
 	 * Returns the sum of two matrixes 
 	 * 
 	 * @param Matrix A 
 	 * @param Matrix B
 	 * 
 	 * @return Matrix R
 	 * 
 	 * @author Isabelly Lima
 	 */

	public static Matrix sumMatrixes(Matrix A, Matrix B)throws Exception{
		if (B.line != A.line || B.column != A.column)
			System.err.println("Wrong!!!!! The number of lines and columns don't match!");
		Matrix R = new Matrix(A.line, A.column);
		for (int i = 1; i <= A.line; i++)
			for (int j = 1; j <= A.column; j++)				
				r.insert(i, j, B.get(i, j)+A.get(i, j));			
		return R;
	}
	
		/**
 	 * Returns the multiplication of two matrices
 	 * 
 	 * @param Matrix m
 	 * @return Matrix aux
 	 * 
 	 * @author Matheus de Souza Oliveira
 	 */

	public static Matrix multiplyMatrices(Matrix m) throws Exception{
        if (this.column == m.line) {
            Matrix aux = new Matrix(this.line, m.column);
            for (int i = 0; i < this.line; i++) {
                for (int j = 0; j < this.column; j++) {
                    Type value = 0;
                    for (int k = 0; k < this.column; k++) {
                        value += this.getValor(i, k) + m.getValor(k, j);
                    }
                    aux.insert(i, j, value);
                }
            }
            return aux;
        } else {
            System.err.println("Error!!! Number of columns of the matrix A must equal the number of rows of the matrix B!");
        }
    }
	
	/**
 	 * Returns the difference of two matrices 
 	 * 
 	 * @param Matrix m 
 	 * 
 	 * @return Matrix aux
 	 * 
 	 * @author Bárbara Feijão
 	 */
	 public static Matrix subMatrices(Matrix m)throws Exception{
     	if (this.column == m.column || this.line == m.line){
			Matrix aux = new Matrix(this.line, this.column);
			for (int i = 0; i < this.line; i++)
				for (int j = 0; j < this.column; j++)				
					r.insert(i, j, this.get(i, j)-m.get(i, j));			
	 	return aux;
     	}else{
        	System.err.println("Wrong!!!!! The number of lines and columns don't match!");
      	}
	}
	
}
